//---------------------------------------------------------------------------
//
// Name:        Genetic TranslationDlg.h
// Author:      Martin Bens
// Created:     19.10.2009 13:42:34
// Description: Genetic_TranslationDlg class declaration
//
//---------------------------------------------------------------------------

#ifndef __GENETIC_TRANSLATIONDLG_h__
#define __GENETIC_TRANSLATIONDLG_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/dialog.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
////Header Include End

////Dialog Style Start
#undef Genetic_TranslationDlg_STYLE
#define Genetic_TranslationDlg_STYLE wxCAPTION | wxSYSTEM_MENU | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

#include "AminosaeuresequenzuebersetzungDlg.h"
#include "DNA - und RNA-SequenzuebersetzungDlg.h"
#include <wx/bitmap.h>

class Genetic_TranslationDlg : public wxDialog
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		Genetic_TranslationDlg(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Genetic Translation"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = Genetic_TranslationDlg_STYLE);
		virtual ~Genetic_TranslationDlg();
	
		void ASS_zu_RNA_DNAClick(wxCommandEvent& event);
		void DNA_RNA_zu_ASSClick(wxCommandEvent& event);
	private:
		//Do not add custom control declarations between 
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		wxStaticBitmap *WxStaticBitmap4;
		wxButton *ASS_zu_RNA_DNA;
		wxButton *DNA_RNA_zu_ASS;
		
		
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			ID_WXSTATICBITMAP4 = 1012,
			ID_DNA_RNA_ZU_ASS = 1002,
			ID_ASS_ZU_RNA_DNA = 1003,
			
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
	
	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

#endif
