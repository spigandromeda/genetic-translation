#include "NSSequenz.h"

void NSSequenz::Leerzeichen_loeschen()
{
    for (unsigned int index=0; index<itsSequenz.Length(); index++)
    {
        char aktuell=itsSequenz.GetChar(index);
        if (aktuell!='A' && aktuell!='C' && aktuell!='G' && aktuell!='U')
        {
            itsSequenz.Remove(index, 1);
            index--;
        }
    }
}

void NSSequenz::SetSequenz(wxString Sequenz)
{
    itsSequenz=Sequenz;
}

wxString NSSequenz::GetSequenz()
{
    return itsSequenz;
}

void NSSequenz::SetGewicht(long int Gewicht)
{
    itsGewicht=Gewicht;
}

long int NSSequenz::GetGewicht()
{
    return itsGewicht;
}

void NSSequenz::DNA_zu_RNA()
{
    itsSequenz.Replace("T", "U", true);
}

void NSSequenz::RNA_zu_DNA()
{
    itsSequenz.Replace("U", "T", true);
}

void NSSequenz::komplementaer()
{
    itsSequenz.Replace("A", "04", true);
    itsSequenz.Replace("G", "03", true);
    itsSequenz.Replace("G", "02", true);
    itsSequenz.Replace("U", "01", true);
}

/*void NSSequenz::UebersetzungAS()
{
    wxString AS_String, Teilstring;
    long int Gewicht;
    wxTextFile AS;
    AS.Open("AS");
    int Beginn=0;
    unsigned int durchlaeufe=itsSequenz.Length()/3;

    for (unsigned int durchlauf=0; durchlauf<durchlaeufe; durchlauf++)
    {
        wxString Zeile_aktuell_String;
        int Zeile_aktuell_Int=1;
        bool pruefung=true;
        while (pruefung)
        {
            Zeile_aktuell_String=AS.GetLine(Zeile_aktuell_Int);
            Teilstring=Zeile_aktuell_String.Mid(0, 3);
            if (itsSequenz.Mid(Beginn, 3)==Teilstring)
            {
                pruefung=false;
                AS_String=Zeile_aktuell_String.Mid(3, 2);
                wxString Gewicht_String=Zeile_aktuell_String.Mid(5,3);
                Gewicht_String.ToLong(&Gewicht);
                itsGewicht=itsGewicht+Gewicht;
            }
            else 
            {
                pruefung=true;
                Zeile_aktuell_Int++;
            }
        }
        Beginn+=2;
        itsSequenz.Replace(Teilstring, AS_String, false);
    }
    AS.Close();
}*/

void NSSequenz::UebersetzungAS()
{
    wxString Teilstring, AS_String (wxT("00"));
    long int Gewicht;
    int Beginn=0;
    unsigned int durchlaeufe=itsSequenz.Length()/3;

    for (unsigned int durchlauf=0; durchlauf<durchlaeufe; durchlauf++)
    {
        wxString Triplett_aktuell;
        Triplett_aktuell=itsSequenz.Mid(Beginn, 3);
        Triplett_aktuell.Replace("A", "25", true);
        Triplett_aktuell.Replace("C", "26", true);
        Triplett_aktuell.Replace("G", "27", true);
        Triplett_aktuell.Replace("U", "28", true);
        wxString Base1, Base2, Base3;
        long int Base1_Zahl, Base2_Zahl, Base3_Zahl;
        Base1=Triplett_aktuell.Mid(0,2);
        Base1.ToLong(&Base1_Zahl);
        Base2=Triplett_aktuell.Mid(2,2);
        Base2.ToLong(&Base2_Zahl);
        Base3=Triplett_aktuell.Mid(4,2);
        Base3.ToLong(&Base3_Zahl);
        switch (Base1_Zahl)
        {
            case 25:
                switch (Base2_Zahl)
                        {
                            case 25: 
                                switch (Base3_Zahl)
                                        {
                                            case 25 | 27: AS_String="18"; break;
                                            case 26 | 28: AS_String="14"; break;
                                        }
                            case 26: AS_String="11"; break;
                            case 27: 
                                switch (Base3_Zahl)
                                        {
                                            case 25 | 27: AS_String="19"; break;
                                            case 26 | 28: AS_String="10"; break;
                                        }
                            case 28: 
                                switch (Base3_Zahl)
                                        {
                                            case 25 | 26 | 28: AS_String="05"; break;
                                            case 27: AS_String="13"; break;
                                        }
                        }
            case 26:
                switch (Base2_Zahl)
                        {
                            case 25: 
                                switch (Base3_Zahl)
                                        {
                                            case 25 | 27: AS_String="17"; break;
                                            case 26 | 28: AS_String="20"; break;
                                        }
                            case 26: AS_String="06"; break;
                            case 27: AS_String="19"; break;
                            case 28: AS_String="04"; break;
                        }
            case 27:
                switch (Base2_Zahl)
                        {
                            case 25: 
                                switch (Base3_Zahl)
                                        {
                                            case 25 | 27: AS_String="15"; break;
                                            case 26 | 28: AS_String="16"; break;
                                        }
                            case 26: AS_String="02"; break;
                            case 27: AS_String="01"; break;
                            case 28: 
                                switch (Base3_Zahl)
                                        {
                                            case 25 | 26 | 28: AS_String="03"; break;
                                            case 27: AS_String="22"; break;
                                        }
                        }
            case 28:
                switch (Base2_Zahl)
                        {
                            case 25: 
                                switch (Base3_Zahl)
                                        {
                                            case 25 | 27: AS_String="11"; break;
                                            case 26 | 28: AS_String="22"; break;
                                        }
                            case 26: AS_String="10"; break;
                            case 27: 
                                switch (Base3_Zahl)
                                        {
                                            case 25: AS_String="21"; break;
                                            case 26 | 28: AS_String="12"; break;
                                            case 27: AS_String="09"; break;
                                        }
                        }
        }
        wxMessageBox(AS_String);
        Beginn+=2;
        Triplett_aktuell.Replace("25", "A", true);
        Triplett_aktuell.Replace("26", "C", true);
        Triplett_aktuell.Replace("27", "G", true);
        Triplett_aktuell.Replace("28", "U", true);
        itsSequenz.Replace(Triplett_aktuell, AS_String, false);
    }
}
                
            
            
wxString NSSequenz::Zahl_zu_Einbuchstabenform()
{
    wxString AS, Teilstring;
    long int AS_Zahl;
    int Beginn=0;
    unsigned int durchlaeufe=itsSequenz.Length()/2;
    
    for (unsigned int durchlauf=0; durchlauf<durchlaeufe; durchlauf++)
    {
        Teilstring=itsSequenz.Mid(Beginn, 2);
        Teilstring.ToLong(&AS_Zahl);
        switch(AS_Zahl)
        {
            case 1: AS="G"; break;
            case 2: AS="A"; break;
            case 3: AS="V"; break;
            case 4: AS="L"; break;
            case 5: AS="I"; break;
            case 6: AS="P"; break;
            case 7: AS="F"; break;
            case 8: AS="Y"; break;
            case 9: AS="W"; break;
            case 10: AS="S"; break;
            case 11: AS="T"; break;
            case 12: AS="C"; break;
            case 13: AS="M"; break;
            case 14: AS="N"; break;
            case 15: AS="Q"; break;
            case 16: AS="D"; break;
            case 17: AS="E"; break;
            case 18: AS="K"; break;
            case 19: AS="R"; break;
            case 20: AS="H"; break;
            case 21: AS="Stopp"; Beginn+=4; break;
            case 22: AS="Start"; Beginn+=4; break;
        }
        Beginn++;
        itsSequenz.Replace(Teilstring, AS, false);
    }
    return itsSequenz;
}

wxString NSSequenz::Zahl_zu_Dreibuchstabenform()
{
    wxString AS, Teilstring;
    long int AS_Zahl;
    int Beginn=0;
    unsigned int durchlaeufe=itsSequenz.Length()/2;

    for (unsigned int durchlauf=0; durchlauf<durchlaeufe; durchlauf++)
    {
        Teilstring=itsSequenz.Mid(Beginn, 2);
        Teilstring.ToLong(&AS_Zahl);

        switch(AS_Zahl)
        {
            case 1: AS="Gly "; break;
            case 2: AS="Ala "; break;
            case 3: AS="Val "; break;
            case 4: AS="Leu "; break;
            case 5: AS="Ile "; break;
            case 6: AS="Pro "; break;
            case 7: AS="Phe "; break;
            case 8: AS="Tyr "; break;
            case 9: AS="Trp "; break;
            case 10: AS="Ser "; break;
            case 11: AS="Thr "; break;
            case 12: AS="Cys "; break;
            case 13: AS="Met "; break;
            case 14: AS="Asn "; break;
            case 15: AS="Glu "; break;
            case 16: AS="Asp "; break;
            case 17: AS="Gln "; break;
            case 18: AS="Lys "; break;
            case 19: AS="Arg "; break;
            case 20: AS="His "; break;
            case 21: AS="Stopp "; Beginn+=2; break;
            case 22: AS="Start "; Beginn+=2; break;
        }
        Beginn+=4;
        itsSequenz.Replace(Teilstring, AS, false);
    }
    return itsSequenz;
}


wxString NSSequenz::Zahl_zu_Trivialname()
{
    wxString AS, Teilstring;
    long int AS_Zahl;
    int Beginn=0;
    unsigned int durchlaeufe=itsSequenz.Length()/2;

    for (unsigned int durchlauf=0; durchlauf<durchlaeufe; durchlauf++)
    {
        Teilstring=itsSequenz.Mid(Beginn, 2);
        Teilstring.ToLong(&AS_Zahl);

        switch(AS_Zahl)
        {
            case 1: AS="Glycin "; Beginn+=7; break;
            case 2: AS="Alanin "; Beginn+=7; break;
            case 3: AS="Valin "; Beginn+=6; break;
            case 4: AS="Leucin "; Beginn+=7; break;
            case 5: AS="Isoleucin "; Beginn+=10; break;
            case 6: AS="Prolin "; Beginn+=7; break;
            case 7: AS="Phenylalanin "; Beginn+=13; break;
            case 8: AS="Tyrosin "; Beginn+=8; break;
            case 9: AS="Tryptophan "; Beginn+=11; break;
            case 10: AS="Serin "; Beginn+=6; break;
            case 11: AS="Threonin "; Beginn+=9; break;
            case 12: AS="Cystein "; Beginn+=8; break;
            case 13: AS="Methionin "; Beginn+=10; break;
            case 14: AS="Asparagin "; Beginn+=10; break;
            case 15: AS="Glutamat "; Beginn+=9; break;
            case 16: AS="Aspartat "; Beginn+=9; break;
            case 17: AS="Glutamin "; Beginn+=9; break;
            case 18: AS="Lysin "; Beginn+=6; break;
            case 19: AS="Arginin "; Beginn+=8; break;
            case 20: AS="Histidin "; Beginn+=9; break;
            case 21: AS="Stopp "; Beginn+=6; break;
            case 22: AS="Start "; Beginn+=6; break;
        }
        itsSequenz.Replace(Teilstring, AS, false);
    }
    return itsSequenz;
}

wxString NSSequenz::Zahl_zu_NSS(bool DNA)
{
    komplementaer();
    itsSequenz.Replace("01", "A", true);
    itsSequenz.Replace("02", "C", true);
    itsSequenz.Replace("03", "G", true);
    itsSequenz.Replace("04", "U", true);
    if (DNA)
    {
        RNA_zu_DNA();
    }
    return itsSequenz;
}   
