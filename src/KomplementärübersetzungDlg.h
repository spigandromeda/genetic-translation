///-----------------------------------------------------------------
///
/// @file      KomplementärübersetzungDlg.h
/// @author    Martin Bens
/// Created:   17.11.2009 12:10:53
/// @section   DESCRIPTION
///            KomplementaeruebersetzungDlg class declaration
///
///------------------------------------------------------------------

#ifndef __KOMPLEMENTAERUEBERSETZUNGDLG_H__
#define __KOMPLEMENTAERUEBERSETZUNGDLG_H__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/dialog.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/richtext/richtextctrl.h>
#include <wx/radiobut.h>
////Header Include End

#include "NSSequenz.h"

////Dialog Style Start
#undef KomplementaeruebersetzungDlg_STYLE
#define KomplementaeruebersetzungDlg_STYLE wxCAPTION | wxSYSTEM_MENU | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class KomplementaeruebersetzungDlg : public wxDialog
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		KomplementaeruebersetzungDlg(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Komplementärübersetzung"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = KomplementaeruebersetzungDlg_STYLE);
		virtual ~KomplementaeruebersetzungDlg();
		void SetText_Eingabe();
		wxString GetText_Eingabe();
		void Uebersetzen_ButtonClick(wxCommandEvent& event);
	
	private:
		//Do not add custom control declarations between 
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxButton *Uebersetzen_Button;
		wxRichTextCtrl *DNA_RNA_Eingabe;
		wxRadioButton *RNA_Radiobutton;
		wxRadioButton *DNA_Radiobutton;
		////GUI Control Declaration End
		
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_UEBERSETZEN_BUTTON = 1004,
			ID_DNA_RNA_EINGABE = 1003,
			ID_RNA_RADIOBUTTON = 1002,
			ID_DNA_RADIOBUTTON = 1001,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
	
	private:
        wxString itsText_Eingabe;
	
	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

#endif
