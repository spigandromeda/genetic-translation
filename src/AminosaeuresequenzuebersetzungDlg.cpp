//---------------------------------------------------------------------------
//
// Name:        Aminos�uresequenz�bersetzungDlg.cpp
// Author:      Martin Bens
// Created:     16.10.2009 09:39:30
// Description: AminosaeuresequenzuebersetzungDlg class implementation
//
//---------------------------------------------------------------------------

#include "AminosaeuresequenzuebersetzungDlg.h"

//Do not add custom headers
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// AminosaeuresequenzuebersetzungDlg
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(AminosaeuresequenzuebersetzungDlg,wxDialog)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(AminosaeuresequenzuebersetzungDlg::OnClose)
	EVT_BUTTON(ID_UEBERSETZUNG_BUTTON,AminosaeuresequenzuebersetzungDlg::Uebersetzung_ButtonClick)
END_EVENT_TABLE()
////Event Table End

AminosaeuresequenzuebersetzungDlg::AminosaeuresequenzuebersetzungDlg(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxDialog(parent, id, title, position, size, style)
{
	CreateGUIControls();
	itsText_Eingabe=ASSequenz_Eingabe->GetValue();
}

AminosaeuresequenzuebersetzungDlg::~AminosaeuresequenzuebersetzungDlg()
{
} 

void AminosaeuresequenzuebersetzungDlg::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End.
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	Trivialname_RadioButton = new wxRadioButton(this, ID_TRIVIALNAME_RADIOBUTTON, wxT("Trivialname"), wxPoint(20, 45), wxSize(125, 17), 0, wxDefaultValidator, wxT("Trivialname_RadioButton"));

	Uebersetzung_Button = new wxButton(this, ID_UEBERSETZUNG_BUTTON, wxT("�bersetzen!"), wxPoint(370, 255), wxSize(100, 30), 0, wxDefaultValidator, wxT("Uebersetzung_Button"));

	wxArrayString arrayStringFor_RNA_DNA_Wahl;
	arrayStringFor_RNA_DNA_Wahl.Add(wxT("�bersetzung in eine RNA-Sequenz"));
	arrayStringFor_RNA_DNA_Wahl.Add(wxT("�bersetzung in eine DNA-Sequenz"));
	RNA_DNA_Wahl = new wxChoice(this, ID_RNA_DNA_WAHL, wxPoint(280, 20), wxSize(190, 21), arrayStringFor_RNA_DNA_Wahl, 0, wxDefaultValidator, wxT("RNA_DNA_Wahl"));
	RNA_DNA_Wahl->SetSelection(0);

	Dreibuchstabenform_RadioButton = new wxRadioButton(this, ID_DREIBUCHSTABENFORM_RADIOBUTTON, wxT("Dreibuchstabenform"), wxPoint(150, 20), wxSize(130, 20), 0, wxDefaultValidator, wxT("Dreibuchstabenform_RadioButton"));

	Einbuchstabenform_RadioButton = new wxRadioButton(this, ID_EINBUCHSTABENFORM_RADIOBUTTON, wxT("Einbuchstabenform"), wxPoint(20, 20), wxSize(120, 20), 0, wxDefaultValidator, wxT("Einbuchstabenform_RadioButton"));
	Einbuchstabenform_RadioButton->SetValue(true);

	ASSequenz_Eingabe = new wxRichTextCtrl(this, ID_ASSEQUENZ_EINGABE, wxT(""), wxPoint(20, 70), wxSize(450, 170), 0, wxDefaultValidator, wxT("ASSequenz_Eingabe"));
	ASSequenz_Eingabe->SetMaxLength(0);
	ASSequenz_Eingabe->SetFocus();
	ASSequenz_Eingabe->SetInsertionPointEnd();

	SetTitle(wxT("Aminos�uresequenz�bersetzung"));
	SetIcon(wxNullIcon);
	SetSize(8,8,500,335);
	Center();
	
	////GUI Items Creation End
}

void AminosaeuresequenzuebersetzungDlg::OnClose(wxCloseEvent& /*event*/)
{
	Destroy();
}

/*
 * Uebersetzung_ButtonClick
 */
void AminosaeuresequenzuebersetzungDlg::Uebersetzung_ButtonClick(wxCommandEvent& event)
{
    SetText_Eingabe();
    bool aktiviert1=Einbuchstabenform_RadioButton->GetValue();
    bool aktiviert2=Dreibuchstabenform_RadioButton->GetValue();
    bool aktiviert3=Trivialname_RadioButton->GetValue();
    ASS Sequenz1;
    Sequenz1.SetSequenz(itsText_Eingabe);
    if (aktiviert1==true and aktiviert2==false and aktiviert3==false)
    {
        Sequenz1.Sequenzuebersetzung_Einbuchstabenform();
    }
    
    else if (aktiviert1==false and aktiviert2==true and aktiviert3==false)
    {
        Sequenz1.Sequenzuebersetzung_Dreibuchstabenform();
    }
    
    else
    {
        Sequenz1.Sequenzuebersetzung_Trivialname();
    } 
    
    int wahl=RNA_DNA_Wahl->GetCurrentSelection();
    if (wahl==1)
    {
        Sequenz1.RNA_zu_DNA();
    }
    
    itsText_Eingabe=Sequenz1.GetSequenz();
    
    ErgebnisDlg Ergebnis_Fenster(this, itsText_Eingabe, 0);
    Ergebnis_Fenster.ShowModal();
	// insert your code here
}

void AminosaeuresequenzuebersetzungDlg::SetText_Eingabe()
{
    itsText_Eingabe=ASSequenz_Eingabe->GetValue();
}   

wxString AminosaeuresequenzuebersetzungDlg::GetText_Eingabe()
{
    return itsText_Eingabe;
}
