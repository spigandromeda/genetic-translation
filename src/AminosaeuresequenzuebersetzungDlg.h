//---------------------------------------------------------------------------
//
// Name:        Aminos�uresequenz�bersetzungDlg.h
// Author:      Martin Bens
// Created:     16.10.2009 09:39:30
// Description: AminosaeuresequenzuebersetzungDlg class declaration
//
//---------------------------------------------------------------------------

#ifndef __AMINOSAEURESEQUENZUEBERSETZUNGDLG_h__
#define __AMINOSAEURESEQUENZUEBERSETZUNGDLG_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/dialog.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/choice.h>
#include <wx/radiobut.h>
#include <wx/richtext/richtextctrl.h>
////Header Include End

#include "ErgebnisDlg.h"
#include "ASS.h"

////Dialog Style Start
#undef AminosaeuresequenzuebersetzungDlg_STYLE
#define AminosaeuresequenzuebersetzungDlg_STYLE wxCAPTION | wxSYSTEM_MENU | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class AminosaeuresequenzuebersetzungDlg : public wxDialog
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		AminosaeuresequenzuebersetzungDlg(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Aminos�uresequenz�bersetzung"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = AminosaeuresequenzuebersetzungDlg_STYLE);
		virtual ~AminosaeuresequenzuebersetzungDlg();
		void Uebersetzung_ButtonClick(wxCommandEvent& event);
		void SetText_Eingabe();
		wxString GetText_Eingabe();
	
	private:
		//Do not add custom control declarations between 
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxRadioButton *Trivialname_RadioButton;
		wxButton *Uebersetzung_Button;
		wxChoice *RNA_DNA_Wahl;
		wxRadioButton *Dreibuchstabenform_RadioButton;
		wxRadioButton *Einbuchstabenform_RadioButton;
		wxRichTextCtrl *ASSequenz_Eingabe;
		////GUI Control Declaration End
		
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_TRIVIALNAME_RADIOBUTTON = 1006,
			ID_UEBERSETZUNG_BUTTON = 1005,
			ID_RNA_DNA_WAHL = 1004,
			ID_DREIBUCHSTABENFORM_RADIOBUTTON = 1003,
			ID_EINBUCHSTABENFORM_RADIOBUTTON = 1002,
			ID_ASSEQUENZ_EINGABE = 1001,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
	
	private:
        wxString itsText_Eingabe;
        
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

#endif
