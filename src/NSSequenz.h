#include <wx/string.h>
#include <wx/msgdlg.h>
#include <wx/textfile.h>

class NSSequenz
{
    private:
    wxString itsSequenz;
    long int itsGewicht;
    
    public:
    void SetSequenz(wxString Sequenz);
    wxString GetSequenz();
    void SetGewicht(long int Gewicht);
    long int GetGewicht();
    void Leerzeichen_loeschen();
    void DNA_zu_RNA();
    void RNA_zu_DNA();
    void komplementaer();
    void UebersetzungAS();
    wxString Zahl_zu_NSS(bool DNA);
    wxString Zahl_zu_Einbuchstabenform();
    wxString Zahl_zu_Dreibuchstabenform();
    wxString Zahl_zu_Trivialname();
};
