//---------------------------------------------------------------------------
//
// Name:        Genetic TranslationFrm.cpp
// Author:      Martin Bens
// Created:     02.11.2009 17:46:33
// Description: Genetic_TranslationFrm class implementation
//
//---------------------------------------------------------------------------

#include "Genetic TranslationFrm.h"

//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// Genetic_TranslationFrm
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(Genetic_TranslationFrm,wxFrame)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(Genetic_TranslationFrm::OnClose)
	EVT_BUTTON(ID_DNA_RNA_ZU_ASS,Genetic_TranslationFrm::DNA_RNA_zu_ASSClick)
	EVT_BUTTON(ID_ASS_ZU_RNA_DNA,Genetic_TranslationFrm::ASS_zu_RNA_DNAClick)
	EVT_BUTTON(ID_DNA_RNA_KOMPLEMENTAER_UEBERSETZEN,Genetic_TranslationFrm::DNA_RNA_komplementaer_uebersetzenClick)
	EVT_BUTTON(ID_DNA_RNA_IN_RNA_DNA_UMWANDELN,Genetic_TranslationFrm::DNA_RNA_in_RNA_DNA_umwandelnClick)
END_EVENT_TABLE()
////Event Table End

Genetic_TranslationFrm::Genetic_TranslationFrm(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxFrame(parent, id, title, position, size, style)
{
	CreateGUIControls();
}

Genetic_TranslationFrm::~Genetic_TranslationFrm()
{
}

void Genetic_TranslationFrm::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	WxMenuBar1 = new wxMenuBar();
	wxMenu *ID_MNU_HILFE_1004_Mnu_Obj = new wxMenu(0);
	WxMenuBar1->Append(ID_MNU_HILFE_1004_Mnu_Obj, wxT("Hilfe"));
	SetMenuBar(WxMenuBar1);

	DNA_RNA_zu_ASS = new wxButton(this, ID_DNA_RNA_ZU_ASS, wxT("DNA - oder RNA - Sequenz in Aminos�uresequenz umwandeln"), wxPoint(23, 550), wxSize(381, 40), 0, wxDefaultValidator, wxT("DNA_RNA_zu_ASS"));

	ASS_zu_RNA_DNA = new wxButton(this, ID_ASS_ZU_RNA_DNA, wxT("Aminos�uresequenz in DNA - oder RNA - Sequenz umwandeln"), wxPoint(427, 550), wxSize(381, 40), 0, wxDefaultValidator, wxT("ASS_zu_RNA_DNA"));

	DNA_RNA_komplementaer_uebersetzen = new wxButton(this, ID_DNA_RNA_KOMPLEMENTAER_UEBERSETZEN, wxT("DNA  - oder RNA - Sequenz komplement�r �bersetzen"), wxPoint(23, 605), wxSize(380, 40), 0, wxDefaultValidator, wxT("DNA_RNA_komplementaer_uebersetzen"));

	DNA_RNA_in_RNA_DNA_umwandeln = new wxButton(this, ID_DNA_RNA_IN_RNA_DNA_UMWANDELN, wxT("DNA - oder RNA  - Sequenz in RNA - oder DNA - Sequenz umwandeln"), wxPoint(428, 605), wxSize(380, 40), 0, wxDefaultValidator, wxT("DNA_RNA_in_RNA_DNA_umwandeln"));

	SetTitle(wxT("Genetic Translation"));
	SetIcon(wxNullIcon);
	SetSize(8,8,839,700);
	Center();
	
	////GUI Items Creation End
	
	wxImage Bild4; Bild4.LoadFile("Molekulargenetik_Programm.bmp", wxBITMAP_TYPE_BMP);
    wxBitmap WxStaticBitmap4_BITMAP(Bild4);
	WxStaticBitmap4 = new wxStaticBitmap(this, ID_WXSTATICBITMAP4, WxStaticBitmap4_BITMAP, wxPoint(-5, -3), wxSize(840, 539));
	WxStaticBitmap4->Enable(false);
}

void Genetic_TranslationFrm::OnClose(wxCloseEvent& event)
{
	Destroy();
}

/*
 * ASS_zu_RNA_DNAClick
 */
void Genetic_TranslationFrm::ASS_zu_RNA_DNAClick(wxCommandEvent& event)
{
    AminosaeuresequenzuebersetzungDlg Window_ASS_zu_RNA_DNA(this);
    Window_ASS_zu_RNA_DNA.ShowModal();
	// insert your code here
}

/*
 * DNA_RNA_zu_ASSClick
 */
void Genetic_TranslationFrm::DNA_RNA_zu_ASSClick(wxCommandEvent& event)
{
    DNA_und_RNA_SequenzuebersetzungDlg Window_DNA_RNA_zu_ASS(this);
    Window_DNA_RNA_zu_ASS.ShowModal();
	// insert your code here
}


/*
 * DNA_RNA_komplementaer_uebersetzenClick
 */
void Genetic_TranslationFrm::DNA_RNA_komplementaer_uebersetzenClick(wxCommandEvent& event)
{
    KomplementaeruebersetzungDlg Window_DNA_RNA_komplementaer_uebersetzen(this);
    Window_DNA_RNA_komplementaer_uebersetzen.ShowModal();
	// insert your code here
}

/*
 * DNA_RNA_in_RNA_DNA_umwandelnClick
 */
void Genetic_TranslationFrm::DNA_RNA_in_RNA_DNA_umwandelnClick(wxCommandEvent& event)
{
    SequenzuebersetzungDlg Window_DNA_RNA_in_RNA_DNA_umwandeln(this);
    Window_DNA_RNA_in_RNA_DNA_umwandeln.ShowModal();
	// insert your code here
}
