//---------------------------------------------------------------------------
//
// Name:        Genetic TranslationDlg.cpp
// Author:      Martin Bens
// Created:     19.10.2009 13:42:34
// Description: Genetic_TranslationDlg class implementation
//
//---------------------------------------------------------------------------

#include "Genetic TranslationDlg.h"
#include <wx/image.h>

//Do not add custom headers
//wxDev-C++ designer will remove them


//----------------------------------------------------------------------------
// Genetic_TranslationDlg
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(Genetic_TranslationDlg,wxDialog)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(Genetic_TranslationDlg::OnClose)
	EVT_BUTTON(ID_ASS_ZU_RNA_DNA,Genetic_TranslationDlg::ASS_zu_RNA_DNAClick)
	EVT_BUTTON(ID_DNA_RNA_ZU_ASS,Genetic_TranslationDlg::DNA_RNA_zu_ASSClick)
END_EVENT_TABLE()
////Event Table End

Genetic_TranslationDlg::Genetic_TranslationDlg(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxDialog(parent, id, title, position, size, style)
{
	CreateGUIControls();
}

Genetic_TranslationDlg::~Genetic_TranslationDlg()
{
} 

void Genetic_TranslationDlg::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End.
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blo
	
    wxImage Bild4; Bild4.LoadFile("Molekulargenetik_Programm.bmp", wxBITMAP_TYPE_BMP);
    wxBitmap WxStaticBitmap4_BITMAP(Bild4);
	WxStaticBitmap4 = new wxStaticBitmap(this, ID_WXSTATICBITMAP4, WxStaticBitmap4_BITMAP, wxPoint(-5, -3), wxSize(725, 465));
	WxStaticBitmap4->Enable(false);

	DNA_RNA_zu_ASS = new wxButton(this, ID_DNA_RNA_ZU_ASS, wxT("DNA - oder RNA - Sequenz in Aminos�uresequenz umwandeln"), wxPoint(20, 477), wxSize(330, 35), 0, wxDefaultValidator, wxT("DNA_RNA_zu_ASS"));

    ASS_zu_RNA_DNA = new wxButton(this, ID_ASS_ZU_RNA_DNA, wxT("Aminos�uresequenz in DNA - oder RNA - Sequenz umwandlen"), wxPoint(370, 477), wxSize(330, 35), 0, wxDefaultValidator, wxT("ASS_zu_RNA_DNA"));

	SetTitle(wxT("Genetic Translation"));
	SetIcon(wxNullIcon);
	SetSize(8,8,721,556);
	Center();
	

}

void Genetic_TranslationDlg::OnClose(wxCloseEvent& /*event*/)
{
	Destroy();
}


/*
 * ASS_zu_RNA_DNAClick
 */
void Genetic_TranslationDlg::ASS_zu_RNA_DNAClick(wxCommandEvent& event)
{
    AminosaeuresequenzuebersetzungDlg Window_ASS_zu_RNA_DNA(this);
    Window_ASS_zu_RNA_DNA.ShowModal();
	// insert your code here
}

/*
 * DNA_RNA_zu_ASSClick
 */
void Genetic_TranslationDlg::DNA_RNA_zu_ASSClick(wxCommandEvent& event)
{
    DNA_und_RNA_SequenzuebersetzungDlg Window_DNA_RNA_zu_ASS(this);
    Window_DNA_RNA_zu_ASS.ShowModal();
	// insert your code here
}
