//---------------------------------------------------------------------------
//
// Name:        ErgebnisDlg.h
// Author:      Martin Bens
// Created:     17.10.2009 22:56:25
// Description: ErgebnisDlg class declaration
//
//---------------------------------------------------------------------------

#ifndef __ERGEBNISDLG_h__
#define __ERGEBNISDLG_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/dialog.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/stattext.h>
#include <wx/filedlg.h>
#include <wx/button.h>
#include <wx/richtext/richtextctrl.h>
////Header Include End

#include <wx/textfile.h>
#include "NSSequenz.h"

////Dialog Style Start
#undef ErgebnisDlg_STYLE
#define ErgebnisDlg_STYLE wxCAPTION | wxSYSTEM_MENU | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class ErgebnisDlg : public wxDialog
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		ErgebnisDlg(wxWindow *parent, wxString Ergebnis_String, long int Gewicht, wxWindowID id = 1, const wxString &title = wxT("‹bersetzung"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = ErgebnisDlg_STYLE);
		virtual ~ErgebnisDlg();
		void Schliessen_ButtonClick(wxCommandEvent& event);
		void Speichern_ButtonClick(wxCommandEvent& event);
		void Drucken_ButtonClick(wxCommandEvent& event);
	
	private:
		//Do not add custom control declarations between 
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxStaticText *Einheit_Text;
		wxStaticText *Ergebnis_StaticText;
		wxStaticText *Gewicht_Text;
		wxFileDialog *Speichern;
		wxButton *Speichern_Button;
		wxButton *Schliessen_Button;
		wxRichTextCtrl *Ergebnis_Text;
		////GUI Control Declaration End
		
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_EINHEIT_TEXT = 1006,
			ID_ERGEBNIS_STATICTEXT = 1005,
			ID_GEWICHT_TEXT = 1004,
			ID_SPEICHERN_BUTTON = 1003,
			ID_SCHLIESSEN_BUTTON = 1002,
			ID_ERGEBNIS_TEXT = 1001,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
	
	private:
        wxString itsErgebnis_String;
        
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

#endif
