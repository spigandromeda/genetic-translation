//---------------------------------------------------------------------------
//
// Name:        DNA - und RNA-SequenzuebersetzungDlg.cpp
// Author:      Martin Bens
// Created:     18.10.2009 16:30:57
// Description: DNA_und_RNA_SequenzuebersetzungDlg class implementation
//
//---------------------------------------------------------------------------

#include "DNA - und RNA-SequenzuebersetzungDlg.h"

//Do not add custom headers
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// DNA_und_RNA_SequenzuebersetzungDlg
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(DNA_und_RNA_SequenzuebersetzungDlg,wxDialog)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(DNA_und_RNA_SequenzuebersetzungDlg::OnClose)
	EVT_BUTTON(ID_UEBERSETZEN_BUTTON,DNA_und_RNA_SequenzuebersetzungDlg::Uebersetzen_ButtonClick)
END_EVENT_TABLE()
////Event Table End

DNA_und_RNA_SequenzuebersetzungDlg::DNA_und_RNA_SequenzuebersetzungDlg(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxDialog(parent, id, title, position, size, style)
{
	CreateGUIControls();
	itsText_Eingabe=DNA_RNA_Eingabe->GetValue();
}

DNA_und_RNA_SequenzuebersetzungDlg::~DNA_und_RNA_SequenzuebersetzungDlg()
{
} 

void DNA_und_RNA_SequenzuebersetzungDlg::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End.
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	DNA_Sequenz_RadioButton = new wxRadioButton(this, ID_DNA_SEQUENZ_RADIOBUTTON, wxT("DNA - Sequenz"), wxPoint(173, 23), wxSize(150, 20), 0, wxDefaultValidator, wxT("DNA_Sequenz_RadioButton"));

	RNA_Sequenz_RadioButton = new wxRadioButton(this, ID_RNA_SEQUENZ_RADIOBUTTON, wxT("RNA - Sequenz"), wxPoint(23, 23), wxSize(150, 20), 0, wxDefaultValidator, wxT("RNA_Sequenz_RadioButton"));
	RNA_Sequenz_RadioButton->SetValue(true);

	Uebersetzen_Button = new wxButton(this, ID_UEBERSETZEN_BUTTON, wxT("�bersetzen!"), wxPoint(392, 300), wxSize(115, 35), 0, wxDefaultValidator, wxT("Uebersetzen_Button"));

	wxArrayString arrayStringFor_Form_Wahl;
	arrayStringFor_Form_Wahl.Add(wxT("Einbuchstabenform"));
	arrayStringFor_Form_Wahl.Add(wxT("Dreibuchstabenform"));
	arrayStringFor_Form_Wahl.Add(wxT("Trivialname"));
	Form_Wahl = new wxChoice(this, ID_FORM_WAHL, wxPoint(323, 23), wxSize(185, 23), arrayStringFor_Form_Wahl, 0, wxDefaultValidator, wxT("Form_Wahl"));
	Form_Wahl->SetSelection(0);

	DNA_RNA_Eingabe = new wxRichTextCtrl(this, ID_DNA_RNATEXT, wxT(""), wxPoint(23, 58), wxSize(485, 230), 0, wxDefaultValidator, wxT("DNA_RNA_Eingabe"));
	DNA_RNA_Eingabe->SetMaxLength(0);
	DNA_RNA_Eingabe->SetFocus();
	DNA_RNA_Eingabe->SetInsertionPointEnd();

	SetTitle(wxT("DNA - und RNA-Sequenz�bersetzung"));
	SetIcon(wxNullIcon);
	SetSize(8,8,549,385);
	Center();
	
	////GUI Items Creation End
}

void DNA_und_RNA_SequenzuebersetzungDlg::OnClose(wxCloseEvent& /*event*/)
{
	Destroy();
}

/*
 * Uebersetzen_ButtonClick
 */
void DNA_und_RNA_SequenzuebersetzungDlg::Uebersetzen_ButtonClick(wxCommandEvent& event)
{
    SetText_Eingabe();
    NSSequenz Sequenz1;
    Sequenz1.SetSequenz(itsText_Eingabe);
    Sequenz1.SetGewicht(0);
    if (DNA_Sequenz_RadioButton->GetValue())
    {
        Sequenz1.DNA_zu_RNA();
    }
    Sequenz1.Leerzeichen_loeschen();
    Sequenz1.UebersetzungAS();
    long int Gewicht=Sequenz1.GetGewicht();
    int wahl=Form_Wahl->GetCurrentSelection();
    switch(wahl)
    {
        case 0: itsText_Eingabe=Sequenz1.Zahl_zu_Einbuchstabenform(); break;
        case 1: itsText_Eingabe=Sequenz1.Zahl_zu_Dreibuchstabenform(); break;
        case 2: itsText_Eingabe=Sequenz1.Zahl_zu_Trivialname(); break;
    }
    ErgebnisDlg Ergebnis_Fenster(this, itsText_Eingabe, Gewicht);
    Ergebnis_Fenster.ShowModal();
	// insert your code here
}

void DNA_und_RNA_SequenzuebersetzungDlg::SetText_Eingabe()
{
    itsText_Eingabe=DNA_RNA_Eingabe->GetValue();
}

wxString DNA_und_RNA_SequenzuebersetzungDlg::GetText_Eingabe()
{
    return itsText_Eingabe;
}


/*
 * DNA_RNA_EingabeReturn
 */
void DNA_und_RNA_SequenzuebersetzungDlg::DNA_RNA_EingabeReturn(wxRichTextEvent& event)
{
	// insert your code here
}

/*
 * DNA_RNA_EingabeEnter
 */
void DNA_und_RNA_SequenzuebersetzungDlg::DNA_RNA_EingabeEnter(wxCommandEvent& event)
{
	// insert your code here
}
