//---------------------------------------------------------------------------
//
// Name:        Genetic TranslationFrm.h
// Author:      Martin Bens
// Created:     02.11.2009 17:46:33
// Description: Genetic_TranslationFrm class declaration
//
//---------------------------------------------------------------------------

#ifndef __GENETIC_TRANSLATIONFRM_h__
#define __GENETIC_TRANSLATIONFRM_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/menu.h>
#include <wx/button.h>
////Header Include End

#include "AminosaeuresequenzuebersetzungDlg.h"
#include "DNA - und RNA-SequenzuebersetzungDlg.h"
#include "KomplementaeruebersetzungDlg.h"
#include "SequenzuebersetzungDlg.h"
#include <wx/bitmap.h>

////Dialog Style Start
#undef Genetic_TranslationFrm_STYLE
#define Genetic_TranslationFrm_STYLE wxCAPTION | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class Genetic_TranslationFrm : public wxFrame
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		Genetic_TranslationFrm(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Genetic Translation"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = Genetic_TranslationFrm_STYLE);
		virtual ~Genetic_TranslationFrm();
		void ASS_zu_RNA_DNAClick(wxCommandEvent& event);
		void DNA_RNA_zu_ASSClick(wxCommandEvent& event);
		void DNA_RNA_komplementaer_uebersetzenClick(wxCommandEvent& event);
		void DNA_RNA_in_RNA_DNA_umwandelnClick(wxCommandEvent& event);
		
	private:
		//Do not add custom control declarations between
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxMenuBar *WxMenuBar1;
		wxButton *DNA_RNA_zu_ASS;
		wxButton *ASS_zu_RNA_DNA;
		wxButton *DNA_RNA_komplementaer_uebersetzen;
		wxButton *DNA_RNA_in_RNA_DNA_umwandeln;
		////GUI Control Declaration End
		wxStaticBitmap *WxStaticBitmap4;
		
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_MNU_HILFE_1004 = 1004,
			
			ID_DNA_RNA_ZU_ASS = 1002,
			ID_ASS_ZU_RNA_DNA = 1003,
			ID_DNA_RNA_KOMPLEMENTAER_UEBERSETZEN = 1005,
			ID_DNA_RNA_IN_RNA_DNA_UMWANDELN = 1006,
			////GUI Enum Control ID End
			ID_WXSTATICBITMAP4 = 1001,
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
		
	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

#endif
