//---------------------------------------------------------------------------
//
// Name:        ErgebnisDlg.cpp
// Author:      Martin Bens
// Created:     17.10.2009 22:56:25
// Description: ErgebnisDlg class implementation
//
//---------------------------------------------------------------------------

#include "ErgebnisDlg.h"

//Do not add custom headers
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// ErgebnisDlg
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(ErgebnisDlg,wxDialog)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(ErgebnisDlg::OnClose)
	EVT_BUTTON(ID_SPEICHERN_BUTTON,ErgebnisDlg::Speichern_ButtonClick)
	EVT_BUTTON(ID_SCHLIESSEN_BUTTON,ErgebnisDlg::Schliessen_ButtonClick)
END_EVENT_TABLE()
////Event Table End

ErgebnisDlg::ErgebnisDlg(wxWindow *parent, wxString Ergebnis_String, long int Gewicht, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxDialog(parent, id, title, position, size, style)
{
	CreateGUIControls();
	itsErgebnis_String=Ergebnis_String;
	Ergebnis_Text->SetValue(itsErgebnis_String);
	if (Gewicht==0)
	{
        Gewicht_Text->Show(false);
        Ergebnis_StaticText->Show(false);
        Einheit_Text->Show(false);
    }
    else
    {
	   wxString Gewicht_String; Gewicht_String<<Gewicht;
	   Ergebnis_StaticText->SetLabel(Gewicht_String);
    }
}

ErgebnisDlg::~ErgebnisDlg()
{
} 

void ErgebnisDlg::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End.
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	Einheit_Text = new wxStaticText(this, ID_EINHEIT_TEXT, wxT("g/mol"), wxPoint(278, 183), wxDefaultSize, 0, wxT("Einheit_Text"));

	Ergebnis_StaticText = new wxStaticText(this, ID_ERGEBNIS_STATICTEXT, wxT("00000000"), wxPoint(220, 183), wxDefaultSize, 0, wxT("Ergebnis_StaticText"));

	Gewicht_Text = new wxStaticText(this, ID_GEWICHT_TEXT, wxT("Molekulargewicht:"), wxPoint(125, 183), wxDefaultSize, 0, wxT("Gewicht_Text"));

	Speichern =  new wxFileDialog(this, wxT("Choose a file"), wxT(""), wxT(""), wxT("*.*"), wxSAVE);

	Speichern_Button = new wxButton(this, ID_SPEICHERN_BUTTON, wxT("Speichern"), wxPoint(120, 205), wxSize(90, 25), 0, wxDefaultValidator, wxT("Speichern_Button"));

	Schliessen_Button = new wxButton(this, ID_SCHLIESSEN_BUTTON, wxT("Schließen"), wxPoint(220, 205), wxSize(90, 25), 0, wxDefaultValidator, wxT("Schliessen_Button"));

	Ergebnis_Text = new wxRichTextCtrl(this, ID_ERGEBNIS_TEXT, wxT(""), wxPoint(20, 20), wxSize(290, 153), 0, wxDefaultValidator, wxT("Ergebnis_Text"));
	Ergebnis_Text->SetMaxLength(0);
	Ergebnis_Text->SetFocus();
	Ergebnis_Text->SetInsertionPointEnd();

	SetTitle(wxT("Übersetzung"));
	SetIcon(wxNullIcon);
	SetSize(8,8,340,280);
	Center();
	
	////GUI Items Creation End
}

void ErgebnisDlg::OnClose(wxCloseEvent& /*event*/)
{
	Destroy();
}

/*
 * Schliessen_ButtonClick
 */
void ErgebnisDlg::Schliessen_ButtonClick(wxCommandEvent& event)
{
    Destroy();
	// insert your code here
}

/*
 * Speichern_ButtonClick
 */
void ErgebnisDlg::Speichern_ButtonClick(wxCommandEvent& event)
{
    wxString Ergebnis_String;
    Speichern->ShowModal();
    wxString Dateiname;
    Dateiname=Speichern->GetFilename();
    wxFile Ergebnis_Datei(Dateiname, wxFile::write);
    Ergebnis_Datei.Open(Dateiname, wxFile::write);
    Ergebnis_Datei.Write(itsErgebnis_String);
	// insert your code here
}

