///-----------------------------------------------------------------
///
/// @file      SequenzuebersetzungDlg.cpp
/// @author    Martin Bens
/// Created:   17.11.2009 15:55:27
/// @section   DESCRIPTION
///            SequenzuebersetzungDlg class implementation
///
///------------------------------------------------------------------

#include "SequenzuebersetzungDlg.h"

//Do not add custom headers
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// SequenzuebersetzungDlg
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(SequenzuebersetzungDlg,wxDialog)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(SequenzuebersetzungDlg::OnClose)
	EVT_BUTTON(ID_UEBERSETZEN_BUTTON,SequenzuebersetzungDlg::Uebersetzen_ButtonClick)
END_EVENT_TABLE()
////Event Table End

SequenzuebersetzungDlg::SequenzuebersetzungDlg(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxDialog(parent, id, title, position, size, style)
{
	CreateGUIControls();
}

SequenzuebersetzungDlg::~SequenzuebersetzungDlg()
{
} 

void SequenzuebersetzungDlg::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End.
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	Uebersetzen_Button = new wxButton(this, ID_UEBERSETZEN_BUTTON, wxT("�bersetzten!"), wxPoint(210, 200), wxSize(90, 35), 0, wxDefaultValidator, wxT("Uebersetzen_Button"));

	DNA_RNA_Eingabe = new wxRichTextCtrl(this, ID_DNA_RNA_EINGABE, wxT(""), wxPoint(20, 60), wxSize(280, 120), 0, wxDefaultValidator, wxT("DNA_RNA_Eingabe"));
	DNA_RNA_Eingabe->SetMaxLength(0);
	DNA_RNA_Eingabe->SetFocus();
	DNA_RNA_Eingabe->SetInsertionPointEnd();

	RNA_Radiobutton = new wxRadioButton(this, ID_RNA_RADIOBUTTON, wxT("RNA"), wxPoint(170, 20), wxSize(120, 20), 0, wxDefaultValidator, wxT("RNA_Radiobutton"));

	DNA_Radiobutton = new wxRadioButton(this, ID_DNA_RADIOBUTTON, wxT("DNA"), wxPoint(20, 20), wxSize(120, 20), 0, wxDefaultValidator, wxT("DNA_Radiobutton"));

	SetTitle(wxT("Sequenz�bersetzung"));
	SetIcon(wxNullIcon);
	SetSize(8,8,335,290);
	Center();
	
	////GUI Items Creation End
}

void SequenzuebersetzungDlg::OnClose(wxCloseEvent& /*event*/)
{
	Destroy();
}

/*
 * Uebersetzen_ButtonClick
 */
void SequenzuebersetzungDlg::Uebersetzen_ButtonClick(wxCommandEvent& event)
{
    SetText_Eingabe();
    NSSequenz Sequenz1;
    Sequenz1.SetSequenz(itsText_Eingabe);
    bool DNA=DNA_Radiobutton->GetValue();
    if (DNA)
    {
        Sequenz1.DNA_zu_RNA();
    }
    else
    {
        Sequenz1.RNA_zu_DNA();
    }
    itsText_Eingabe=Sequenz1.GetSequenz();
    ErgebnisDlg Ergebnis_Fenster(this, itsText_Eingabe, 0);
    Ergebnis_Fenster.ShowModal();
	// insert your code here
}

void SequenzuebersetzungDlg::SetText_Eingabe()
{
    itsText_Eingabe=DNA_RNA_Eingabe->GetValue();
}

wxString SequenzuebersetzungDlg::GetText_Eingabe()
{
    return itsText_Eingabe;
}

