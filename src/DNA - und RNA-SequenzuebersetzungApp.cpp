//---------------------------------------------------------------------------
//
// Name:        DNA - und RNA-SequenzuebersetzungApp.cpp
// Author:      Martin Bens
// Created:     18.10.2009 16:30:56
// Description: 
//
//---------------------------------------------------------------------------

#include "DNA - und RNA-SequenzuebersetzungApp.h"
#include "DNA - und RNA-SequenzuebersetzungDlg.h"

IMPLEMENT_APP(DNA_und_RNA_SequenzuebersetzungDlgApp)

bool DNA_und_RNA_SequenzuebersetzungDlgApp::OnInit()
{
	DNA_und_RNA_SequenzuebersetzungDlg* dialog = new DNA_und_RNA_SequenzuebersetzungDlg(NULL);
	SetTopWindow(dialog);
	dialog->Show(true);		
	return true;
}
 
int DNA_und_RNA_SequenzuebersetzungDlgApp::OnExit()
{
	return 0;
}
