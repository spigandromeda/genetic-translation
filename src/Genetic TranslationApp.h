//---------------------------------------------------------------------------
//
// Name:        Genetic TranslationApp.h
// Author:      Martin Bens
// Created:     19.10.2009 13:42:34
// Description: 
//
//---------------------------------------------------------------------------

#ifndef __GENETIC_TRANSLATIONDLGApp_h__
#define __GENETIC_TRANSLATIONDLGApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class Genetic_TranslationFrmApp : public wxApp
{
	public:
		bool OnInit();
		int OnExit();
};

#endif
