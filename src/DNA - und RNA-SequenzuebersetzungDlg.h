//---------------------------------------------------------------------------
//
// Name:        DNA - und RNA-SequenzuebersetzungDlg.h
// Author:      Martin Bens
// Created:     18.10.2009 16:30:57
// Description: DNA_und_RNA_SequenzuebersetzungDlg class declaration
//
//---------------------------------------------------------------------------

#ifndef __DNA_UND_RNA_SEQUENZUEBERSETZUNGDLG_h__
#define __DNA_UND_RNA_SEQUENZUEBERSETZUNGDLG_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/dialog.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/radiobut.h>
#include <wx/button.h>
#include <wx/choice.h>
#include <wx/richtext/richtextctrl.h>
////Header Include End

#include "ErgebnisDlg.h"

////Dialog Style Start
#undef DNA_und_RNA_SequenzuebersetzungDlg_STYLE
#define DNA_und_RNA_SequenzuebersetzungDlg_STYLE wxCAPTION | wxSYSTEM_MENU | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class DNA_und_RNA_SequenzuebersetzungDlg : public wxDialog
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		DNA_und_RNA_SequenzuebersetzungDlg(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("DNA - und RNA-Sequenz�bersetzung"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = DNA_und_RNA_SequenzuebersetzungDlg_STYLE);
		virtual ~DNA_und_RNA_SequenzuebersetzungDlg();
		void Uebersetzen_ButtonClick(wxCommandEvent& event);
		void SetText_Eingabe();
		wxString GetText_Eingabe();
		void DNA_RNA_EingabeReturn(wxRichTextEvent& event);
		void DNA_RNA_EingabeEnter(wxCommandEvent& event);
	
	private:
		//Do not add custom control declarations between 
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxRadioButton *DNA_Sequenz_RadioButton;
		wxRadioButton *RNA_Sequenz_RadioButton;
		wxButton *Uebersetzen_Button;
		wxChoice *Form_Wahl;
		wxRichTextCtrl *DNA_RNA_Eingabe;
		////GUI Control Declaration End
		
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_DNA_SEQUENZ_RADIOBUTTON = 1005,
			ID_RNA_SEQUENZ_RADIOBUTTON = 1004,
			ID_UEBERSETZEN_BUTTON = 1003,
			ID_FORM_WAHL = 1002,
			ID_DNA_RNATEXT = 1001,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
	
	private:
        wxString itsText_Eingabe;
        
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

#endif
