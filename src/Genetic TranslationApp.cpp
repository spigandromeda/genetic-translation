//---------------------------------------------------------------------------
//
// Name:        Genetic TranslationApp.cpp
// Author:      Martin Bens
// Created:     19.10.2009 13:42:34
// Description: 
//
//---------------------------------------------------------------------------

#include "Genetic TranslationApp.h"
#include "Genetic TranslationFrm.h"

IMPLEMENT_APP(Genetic_TranslationFrmApp)

bool Genetic_TranslationFrmApp::OnInit()
{
	Genetic_TranslationFrm* frame = new Genetic_TranslationFrm(NULL);
	SetTopWindow(frame);
	frame->Show(true);		
	return true;
}
 
int Genetic_TranslationFrmApp::OnExit()
{
	return 0;
}
