#include "ASS.h"

wxString ASS::GetSequenz()
{
    return itsSequenz;
}

void ASS::SetSequenz(wxString Sequenz)
{
    itsSequenz=Sequenz;
}

short int ASS::GetForm()
{
    return itsForm;
}

void ASS::SetForm(short int Form)
{
    itsForm=Form;
}

void ASS::Sequenzuebersetzung_Einbuchstabenform()
{
    itsSequenz.Replace("G", "333 ", true);
    itsSequenz.Replace("A", "323 ", true);
    itsSequenz.Replace("V", "341 ", true);
    itsSequenz.Replace("L", "443 ", true);
    itsSequenz.Replace("I", "141 ", true);
    itsSequenz.Replace("P", "223 ", true);
    itsSequenz.Replace("F", "442 ", true);
    itsSequenz.Replace("Y", "412 ", true);
    itsSequenz.Replace("W", "433 ", true);
    itsSequenz.Replace("S", "134 ", true);
    itsSequenz.Replace("T", "123 ", true);
    itsSequenz.Replace("C", "432 ", true);
    itsSequenz.Replace("M", "143 ", true);
    itsSequenz.Replace("N", "112 ", true);
    itsSequenz.Replace("Q", "313 ", true);
    itsSequenz.Replace("D", "312 ", true);
    itsSequenz.Replace("E", "213 ", true);
    itsSequenz.Replace("K", "113 ", true);
    itsSequenz.Replace("R", "233 ", true);
    itsSequenz.Replace("H", "212 ", true);
    
    Zahlen_zu_Buchstaben();
}

void ASS::Sequenzuebersetzung_Dreibuchstabenform()
{
    itsSequenz.Replace("Gly", "333 ", true);
    itsSequenz.Replace("Ala", "323 ", true);
    itsSequenz.Replace("Val", "341 ", true);
    itsSequenz.Replace("Leu", "443 ", true);
    itsSequenz.Replace("Ile", "141 ", true);
    itsSequenz.Replace("Pro", "223 ", true);
    itsSequenz.Replace("Phy", "442 ", true);
    itsSequenz.Replace("Tyr", "412 ", true);
    itsSequenz.Replace("Trp", "433 ", true);
    itsSequenz.Replace("Ser", "134 ", true);
    itsSequenz.Replace("Thr", "123 ", true);
    itsSequenz.Replace("Cys", "432 ", true);
    itsSequenz.Replace("Met", "143 ", true);
    itsSequenz.Replace("Asn", "112 ", true);
    itsSequenz.Replace("Gln", "313 ", true);
    itsSequenz.Replace("Asp", "312 ", true);
    itsSequenz.Replace("Glu", "213 ", true);
    itsSequenz.Replace("Lys", "113 ", true);
    itsSequenz.Replace("Arg", "233 ", true);
    itsSequenz.Replace("His", "212 ", true);
    
    Zahlen_zu_Buchstaben();
}

void ASS::Sequenzuebersetzung_Trivialname()
{
    itsSequenz.Replace("Glycin", "333", true);
    itsSequenz.Replace("Alanin", "323", true);
    itsSequenz.Replace("Valin", "341", true);
    itsSequenz.Replace("Leucin", "443", true);
    itsSequenz.Replace("Isoleucin", "141", true);
    itsSequenz.Replace("Prolin", "223", true);
    itsSequenz.Replace("Phenylalanin", "442", true);
    itsSequenz.Replace("Tyrosin", "412", true);
    itsSequenz.Replace("Tryptophan", "433", true);
    itsSequenz.Replace("Serin", "134", true);
    itsSequenz.Replace("Threonin", "123", true);
    itsSequenz.Replace("Cystein", "432", true);
    itsSequenz.Replace("Methionin", "143", true);
    itsSequenz.Replace("Asparagin", "112", true);
    itsSequenz.Replace("Glutamin", "313", true);
    itsSequenz.Replace("Aspartat", "312", true);
    itsSequenz.Replace("Glutamat", "213", true);
    itsSequenz.Replace("Lysin", "113", true);
    itsSequenz.Replace("Arginin", "233", true);
    itsSequenz.Replace("Histidin", "212", true);

    Zahlen_zu_Buchstaben();
}

void ASS::Zahlen_zu_Buchstaben()
{
    itsSequenz.Replace("1", "A", true);
    itsSequenz.Replace("2", "C", true);
    itsSequenz.Replace("3", "G", true);
    itsSequenz.Replace("4", "U", true);
}

void ASS::RNA_zu_DNA()
{
    itsSequenz.Replace("U", "T", true);
}
