#include <wx/string.h>

class ASS
{   
    private:
    wxString itsSequenz;
    unsigned short itsForm;
    
    public:        
    void Sequenzuebersetzung_Einbuchstabenform();
    void Sequenzuebersetzung_Dreibuchstabenform();
    void Sequenzuebersetzung_Trivialname();
    void Zahlen_zu_Buchstaben();
    void RNA_zu_DNA();
    
    wxString GetSequenz();
    void SetSequenz(wxString Sequenz);
    
    short int GetForm();
    void SetForm(short int Form);
};
